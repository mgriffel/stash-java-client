package com.atlassian.stash.rest.client.core.parser;

import com.atlassian.stash.rest.client.api.entity.Project;
import com.atlassian.stash.rest.client.core.entity.Link;
import com.google.common.base.Function;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.List;

import static com.atlassian.stash.rest.client.core.parser.ParserUtil.linkToHref;
import static com.atlassian.stash.rest.client.core.parser.Parsers.linkParser;
import static com.atlassian.stash.rest.client.core.parser.Parsers.listParser;
import static com.google.common.collect.Iterables.getFirst;
import static com.google.common.collect.Iterables.transform;

class ProjectParser implements Function<JsonElement, Project> {
    @Override
    public Project apply(final JsonElement json) {
        JsonObject jsonObject = json.getAsJsonObject();
        String selfUrl = null;

        if (jsonObject.has("links")) {
            JsonObject links = jsonObject.getAsJsonObject("links");

            List<Link> selfLinks = listParser(linkParser("href", null)).apply(links.get("self"));
            selfUrl = getFirst(transform(selfLinks, linkToHref()), selfUrl);
        }

        return new Project(
                jsonObject.get("key").getAsString(),
                jsonObject.get("id").getAsLong(),
                jsonObject.get("name").getAsString(),
                jsonObject.has("description") ? jsonObject.get("description").getAsString() : null,
                jsonObject.has("public") && jsonObject.get("public").getAsBoolean(),
                jsonObject.has("isPersonal") && jsonObject.get("isPersonal").getAsBoolean(),
                jsonObject.get("type").getAsString(),
                selfUrl
                );
    }

}
