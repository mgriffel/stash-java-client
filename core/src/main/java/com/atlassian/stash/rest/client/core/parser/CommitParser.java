package com.atlassian.stash.rest.client.core.parser;

import com.atlassian.stash.rest.client.api.entity.Commit;
import com.google.common.base.Function;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import java.util.ArrayList;
import java.util.List;

class CommitParser implements Function<JsonElement, Commit> {

   @Override
   public Commit apply(final JsonElement json) {
      JsonObject jsonObject = json.getAsJsonObject();

      List<Commit> parents = new ArrayList<Commit>();

      JsonArray asJsonArray = jsonObject.get("parents").getAsJsonArray();
      for (int i = 0; i < asJsonArray.size(); i++) {
         JsonElement el = asJsonArray.get(i);
         JsonObject asJsonObject = el.getAsJsonObject();
         Commit commit = new Commit(
               asJsonObject.get("id").getAsString(),
               asJsonObject.get("displayId").getAsString(),
                 null, null, null, null,null, null);
         parents.add(commit);
      }

      JsonObject author = jsonObject.get("author").getAsJsonObject();
      JsonObject committer = jsonObject.get("committer").getAsJsonObject();
      return new Commit(
              jsonObject.get("id").getAsString(),
              jsonObject.get("displayId").getAsString(),
              jsonObject.get("authorTimestamp").getAsLong(),
              jsonObject.get("committerTimestamp").getAsLong(),
              author.get("name").getAsString(),
              committer.get("name").getAsString(),
              jsonObject.get("message").getAsString(),
              parents);
   }
}
