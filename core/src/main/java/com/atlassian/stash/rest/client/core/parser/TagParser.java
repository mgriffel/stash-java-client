package com.atlassian.stash.rest.client.core.parser;

import com.atlassian.stash.rest.client.api.entity.Tag;
import com.google.common.base.Function;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

class TagParser implements Function<JsonElement, Tag> {
   @Override
   public Tag apply(final JsonElement json) {
      JsonObject jsonObject = json.getAsJsonObject();
      return new Tag(
            jsonObject.get("id").getAsString(),
            jsonObject.get("displayId").getAsString(),
            jsonObject.get("latestChangeset").getAsString(),
            jsonObject.get("latestCommit").getAsString());
   }
}
