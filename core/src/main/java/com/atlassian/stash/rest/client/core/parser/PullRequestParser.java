package com.atlassian.stash.rest.client.core.parser;

import com.atlassian.stash.rest.client.api.entity.PullRequest;
import com.google.common.base.Function;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

public class PullRequestParser implements Function<JsonElement, PullRequest> {
   @Override
   public PullRequest apply(final JsonElement json) {
      JsonObject jsonObject = json.getAsJsonObject();

      String authorName = getAuthorName(jsonObject);
      String fromRefId = getFromRefId(jsonObject);
      return new PullRequest(
              jsonObject.get("id").getAsString(),
              getTitle(jsonObject),
              jsonObject.get("state").getAsString(),
              jsonObject.get("createdDate").getAsLong(),
              jsonObject.get("updatedDate").getAsLong(),
              fromRefId,
              jsonObject.get("locked").getAsBoolean(),
              authorName,
              getLastCommitId(jsonObject));
   }

   private String getTitle(JsonObject jsonObject) {
     return jsonObject.has("title") ? jsonObject.get("title").getAsString() : "";
   }

   private String getFromRefId(JsonObject jsonObject) {
      JsonObject fromRef = jsonObject.get("fromRef").getAsJsonObject();
      return fromRef == null ? null : fromRef.get("displayId").getAsString();
   }

   private String getLastCommitId(JsonObject jsonObject) {
      JsonObject fromRef = jsonObject.get("fromRef").getAsJsonObject();
      return fromRef == null ? null : fromRef.get("latestCommit").getAsString();
   }

   private String getAuthorName(JsonObject jsonObject) {
      JsonObject author = jsonObject.get("author").getAsJsonObject();
      JsonObject user = author == null ? null : author.get("user").getAsJsonObject();
      return user == null ? null : user.get("name").getAsString();
   }
}
