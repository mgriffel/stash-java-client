package com.atlassian.stash.rest.client.api.entity;

import java.util.List;

public class Commit {
   private final String id;
   private final String displayId;
   private final Long authorTimestamp;
   private final Long committerTimestamp;
   private final String authorName;
   private final String committerName;
   private final String message;
   private final List<Commit> parents;

   public Commit(String id, String displayId, Long authorTimestamp, Long committerTimestamp, String authorName, String committerName, String message, List<Commit> parents) {
      this.id = id;
      this.displayId = displayId;
      this.authorTimestamp = authorTimestamp;
      this.committerTimestamp = committerTimestamp;
      this.authorName = authorName;
      this.committerName = committerName;
      this.message = message;
      this.parents = parents;
   }

   public String getDisplayId() {
      return displayId;
   }

   public String getId() {
      return id;
   }

   public String getAuthorName() {
      return authorName;
   }

   public String getCommitterName() {
      return committerName;
   }

   public Long getAuthorTimestamp() {
      return authorTimestamp;
   }

   public Long getCommitterTimestamp() {
      return committerTimestamp;
   }

   public String getMessage() {
      return message;
   }

   public List<Commit> getParents() {
      return parents;
   }
}
