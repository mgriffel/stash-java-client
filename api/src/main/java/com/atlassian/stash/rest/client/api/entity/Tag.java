package com.atlassian.stash.rest.client.api.entity;

public class Tag {

   private String id;
   private String displayId;
   private String latestChangeset;
   private String lastestCommit;

   public Tag(String id, String displayId, String latestChangeset, String lastestCommit) {
      super();
      this.id = id;
      this.displayId = displayId;
      this.latestChangeset = latestChangeset;
      this.lastestCommit = lastestCommit;
   }

   public String getId() {
      return id;
   }

   public void setId(String id) {
      this.id = id;
   }

   public String getDisplayId() {
      return displayId;
   }

   public void setDisplayId(String displayId) {
      this.displayId = displayId;
   }

   public String getLatestChangeset() {
      return latestChangeset;
   }

   public void setLatestChangeset(String latestChangeset) {
      this.latestChangeset = latestChangeset;
   }

   public String getLastestCommit() {
      return lastestCommit;
   }

   public void setLastestCommit(String lastestCommit) {
      this.lastestCommit = lastestCommit;
   }

}
