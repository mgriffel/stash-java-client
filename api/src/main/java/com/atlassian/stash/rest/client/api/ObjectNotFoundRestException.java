package com.atlassian.stash.rest.client.api;

import java.util.List;

public class ObjectNotFoundRestException extends StashRestException {
   public ObjectNotFoundRestException(List<StashError> errors, int statusCode, String statusMessage, String responseBody) {
      super(errors, statusCode, statusMessage, responseBody);
   }
}
