package com.atlassian.stash.rest.client.api.entity;

public class PullRequest {
   private final String id;
   private final String title;
   private final String state;
   private final Long createdDate;
   private final Long updatedDate;
   private final String fromRefId;
   private final Boolean locked;
   private final String authorName;
   private final String lastCommitId;

   public PullRequest(String id, String title, String state, Long createdDate, Long updatedDate, String fromRefId, Boolean locked, String authorName, String lastCommitId) {
      this.id = id;
      this.title = title;
      this.state = state;
      this.createdDate = createdDate;
      this.updatedDate = updatedDate;
      this.fromRefId = fromRefId;
      this.locked = locked;
      this.authorName = authorName;
      this.lastCommitId = lastCommitId;
   }

   public String getId() {
      return id;
   }

   public String getTitle() {
      return title;
   }

   public String getState() {
      return state;
   }

   public Long getCreatedDate() {
      return createdDate;
   }

   public Long getUpdatedDate() {
      return updatedDate;
   }

   public String getFromRefId() {
      return fromRefId;
   }

   public Boolean getLocked() {
      return locked;
   }

   public String getAuthorName() {
      return authorName;
   }

   public String getLastCommitId() {
      return lastCommitId;
   }
}
