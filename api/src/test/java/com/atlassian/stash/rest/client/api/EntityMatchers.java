package com.atlassian.stash.rest.client.api;

import com.atlassian.stash.rest.client.api.entity.Branch;
import com.atlassian.stash.rest.client.api.entity.Page;
import com.atlassian.stash.rest.client.api.entity.Project;
import com.atlassian.stash.rest.client.api.entity.Repository;
import com.atlassian.stash.rest.client.api.entity.UserSshKey;
import com.google.common.collect.Maps;
import org.hamcrest.Matcher;

import java.util.List;
import java.util.Map;

public class EntityMatchers {

    public static ProjectMatcherBuilder project() {
        return new ProjectMatcherBuilder();
    }

    public static <T> PageMatcherBuilder<T> page(@SuppressWarnings("unused") Class<T> typeHint) {
        return page();
    }

    public static <T> PageMatcherBuilder<T> page() {
        return new PageMatcherBuilder<T>();
    }

    public static RepositoryMatcherBuilder repository() {
        return new RepositoryMatcherBuilder();
    }

    public static BranchMatcherBuilder branch() {
        return new BranchMatcherBuilder();
    }

    public static UserSshKeyMatcherBuilder userSshKey() {
        return new UserSshKeyMatcherBuilder();
    }

    static public class ProjectMatcherBuilder extends PropertyMatcherBuilder<Project, ProjectMatcherBuilder> {

        ProjectMatcherBuilder() {
        }

        public ProjectMatcherBuilder key(Matcher<String> matcher) {
            return put("key", matcher);
        }

        public ProjectMatcherBuilder id(Matcher<Long> matcher) {
            return put("id", matcher);
        }

        public ProjectMatcherBuilder name(Matcher<String> matcher) {
            return put("name", matcher);
        }

        public ProjectMatcherBuilder description(Matcher<String> matcher) {
            return put("description", matcher);
        }

        public ProjectMatcherBuilder isPublic(Matcher<Boolean> matcher) {
            return put("public", matcher);
        }

        public ProjectMatcherBuilder isPersonal(Matcher<Boolean> matcher) {
            return put("personal", matcher);
        }

        public ProjectMatcherBuilder type(Matcher<String> matcher) {
            return put("type", matcher);
        }

        public ProjectMatcherBuilder selfUrl(Matcher<String> matcher) {
            return put("selfUrl", matcher);
        }
    }

    static public class PageMatcherBuilder<T> extends PropertyMatcherBuilder<Page<T>, PageMatcherBuilder<T>> {
        PageMatcherBuilder() {
        }

        public PageMatcherBuilder<T> size(Matcher<Integer> matcher) {
            return put("size", matcher);
        }

        public PageMatcherBuilder<T> limit(Matcher<Integer> matcher) {
            return put("limit", matcher);
        }

        public PageMatcherBuilder<T> values(Matcher<? extends List<T>> matcher) {
            return put("values", matcher);
        }
    }

    static public class RepositoryMatcherBuilder extends PropertyMatcherBuilder<Repository, RepositoryMatcherBuilder> {
        RepositoryMatcherBuilder() {
        }

        public RepositoryMatcherBuilder slug(Matcher<String> matcher) {
            return put("slug", matcher);
        }

        public RepositoryMatcherBuilder id(Matcher<Integer> matcher) {
            return put("id", matcher);
        }

        public RepositoryMatcherBuilder name(Matcher<String> matcher) {
            return put("name", matcher);
        }

        public RepositoryMatcherBuilder isPublic(Matcher<Boolean> matcher) {
            return put("public", matcher);
        }

        public RepositoryMatcherBuilder sshCloneUrl(Matcher<String> matcher) {
            return put("sshCloneUrl", matcher);
        }

        public RepositoryMatcherBuilder httpCloneUrl(Matcher<String> matcher) {
            return put("httpCloneUrl", matcher);
        }

        public RepositoryMatcherBuilder selfUrl(Matcher<String> matcher) {
            return put("selfUrl", matcher);
        }

        public RepositoryMatcherBuilder project(Matcher<Project> matcher) {
            return put("project", matcher);
        }

        public RepositoryMatcherBuilder origin(Matcher<Repository> matcher) {
            return put("origin", matcher);
        }
    }

    static public class BranchMatcherBuilder extends PropertyMatcherBuilder<Branch, BranchMatcherBuilder> {
        BranchMatcherBuilder() {
        }

        public BranchMatcherBuilder isDefault(Matcher<Boolean> matcher) {
            return put("default", matcher);
        }

        public BranchMatcherBuilder id(Matcher<String> matcher) {
            return put("id", matcher);
        }

        public BranchMatcherBuilder displayId(Matcher<String> matcher) {
            return put("displayId", matcher);
        }

        public BranchMatcherBuilder latestChangeset(Matcher<String> matcher) {
            return put("latestChangeset", matcher);
        }
    }

    static public class UserSshKeyMatcherBuilder extends PropertyMatcherBuilder<UserSshKey, UserSshKeyMatcherBuilder> {
        UserSshKeyMatcherBuilder() {
        }

        public UserSshKeyMatcherBuilder id(Matcher<Long> matcher) {
            return put("id", matcher);
        }

        public UserSshKeyMatcherBuilder text(Matcher<String> matcher) {
            return put("text", matcher);
        }

        public UserSshKeyMatcherBuilder label(Matcher<String> matcher) {
            return put("label", matcher);
        }
    }

    static class PropertyMatcherBuilder<T, B extends PropertyMatcherBuilder> {
        private Map<String, Matcher<?>> propertyNameAndMatchers = Maps.newLinkedHashMap();

        PropertyMatcherBuilder() {
        }

        @SuppressWarnings("unchecked")
        public B put(String property, Matcher<?> matcher) {
            propertyNameAndMatchers.put(property, matcher);
            return (B) this;
        }

        public Matcher<T> build() {
            return new PropertyMatcher<T>(propertyNameAndMatchers);
        }
    }
}
